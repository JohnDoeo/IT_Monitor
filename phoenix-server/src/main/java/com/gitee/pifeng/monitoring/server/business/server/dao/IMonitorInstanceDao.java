package com.gitee.pifeng.monitoring.server.business.server.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.pifeng.monitoring.server.business.server.entity.MonitorInstance;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 应用实例数据访问对象
 * </p>
 *
 * @author 皮锋
 * @custom.date 2020/5/10 22:53
 */
@Mapper
public interface IMonitorInstanceDao extends BaseMapper<MonitorInstance> {
}
