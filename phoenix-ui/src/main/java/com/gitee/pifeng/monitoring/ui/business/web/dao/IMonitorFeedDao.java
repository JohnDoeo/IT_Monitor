package com.gitee.pifeng.monitoring.ui.business.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.pifeng.monitoring.ui.business.web.entity.MonitorFeed;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface IMonitorFeedDao extends BaseMapper<MonitorFeed> {
}
