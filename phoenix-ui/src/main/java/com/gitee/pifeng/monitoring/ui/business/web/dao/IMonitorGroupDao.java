package com.gitee.pifeng.monitoring.ui.business.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.pifeng.monitoring.ui.business.web.entity.MonitorGroup;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 监控分组表数据访问对象
 * </p>
 *
 * @author 皮锋
 * @custom.date 2021-12-23
 */
@Mapper
public interface IMonitorGroupDao extends BaseMapper<MonitorGroup> {

}
