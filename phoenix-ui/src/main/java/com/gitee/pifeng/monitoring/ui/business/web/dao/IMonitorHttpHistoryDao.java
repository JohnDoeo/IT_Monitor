package com.gitee.pifeng.monitoring.ui.business.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.pifeng.monitoring.ui.business.web.entity.MonitorHttpHistory;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * HTTP信息历史记录数据访问对象
 * </p>
 *
 * @author 皮锋
 * @custom.date 2022-04-12
 */
@Mapper
public interface IMonitorHttpHistoryDao extends BaseMapper<MonitorHttpHistory> {

}
