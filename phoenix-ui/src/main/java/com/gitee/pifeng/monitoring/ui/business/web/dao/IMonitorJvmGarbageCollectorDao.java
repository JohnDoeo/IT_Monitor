package com.gitee.pifeng.monitoring.ui.business.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.pifeng.monitoring.ui.business.web.entity.MonitorJvmGarbageCollector;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * java虚拟机GC信息数据访问对象
 * </p>
 *
 * @author 皮锋
 * @custom.date 2020/9/6 19:59
 */
@Mapper
public interface IMonitorJvmGarbageCollectorDao extends BaseMapper<MonitorJvmGarbageCollector> {

}
