package com.gitee.pifeng.monitoring.ui.business.web.dao;

import com.gitee.pifeng.monitoring.ui.business.web.entity.MonitorNetHistory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 网络信息历史记录数据访问对象
 * </p>
 *
 * @author 皮锋
 * @custom.date 2022-03-20
 */
@Mapper
public interface IMonitorNetHistoryDao extends BaseMapper<MonitorNetHistory> {

}
