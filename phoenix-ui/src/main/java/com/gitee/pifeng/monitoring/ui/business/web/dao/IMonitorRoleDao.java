package com.gitee.pifeng.monitoring.ui.business.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.pifeng.monitoring.ui.business.web.entity.MonitorRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 监控用户角色数据访问对象
 * </p>
 *
 * @author 皮锋
 * @custom.date 2020年3月7日 下午5:03:49
 */
@Mapper
public interface IMonitorRoleDao extends BaseMapper<MonitorRole> {

}
