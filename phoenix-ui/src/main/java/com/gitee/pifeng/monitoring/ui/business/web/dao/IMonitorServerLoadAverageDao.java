package com.gitee.pifeng.monitoring.ui.business.web.dao;

import com.gitee.pifeng.monitoring.ui.business.web.entity.MonitorServerLoadAverage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 服务器平均负载数据访问对象
 * </p>
 *
 * @author 皮锋
 * @custom.date 2022-06-17
 */
@Mapper
public interface IMonitorServerLoadAverageDao extends BaseMapper<MonitorServerLoadAverage> {

}
