package com.gitee.pifeng.monitoring.ui.business.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.pifeng.monitoring.ui.business.web.entity.MonitorServerMemory;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 服务器内存数据访问对象
 * </p>
 *
 * @author 皮锋
 * @custom.date 2020/9/4 20:50
 */
@Mapper
public interface IMonitorServerMemoryDao extends BaseMapper<MonitorServerMemory> {
}
