package com.gitee.pifeng.monitoring.ui.business.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.pifeng.monitoring.ui.business.web.entity.MonitorServerPowerSources;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 服务器电池数据访问对象
 * </p>
 *
 * @author 皮锋
 * @custom.date 2021-01-15
 */
@Mapper
public interface IMonitorServerPowerSourcesDao extends BaseMapper<MonitorServerPowerSources> {

}
