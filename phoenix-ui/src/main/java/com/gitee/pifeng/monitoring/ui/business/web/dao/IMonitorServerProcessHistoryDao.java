package com.gitee.pifeng.monitoring.ui.business.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.pifeng.monitoring.ui.business.web.entity.MonitorServerProcessHistory;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 服务器进程历史记录信息数据访问对象
 * </p>
 *
 * @author 皮锋
 * @custom.date 2021-09-15
 */
@Mapper
public interface IMonitorServerProcessHistoryDao extends BaseMapper<MonitorServerProcessHistory> {

}
