package com.gitee.pifeng.monitoring.ui.business.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.pifeng.monitoring.ui.business.web.entity.MonitorTcpHistory;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * TCP信息历史记录数据访问对象
 * </p>
 *
 * @author 皮锋
 * @custom.date 2022-03-16
 */
@Mapper
public interface IMonitorTcpHistoryDao extends BaseMapper<MonitorTcpHistory> {

}
