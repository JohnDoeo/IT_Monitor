package com.gitee.pifeng.monitoring.ui.business.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("MONITOR_FEED")
@ApiModel(value = "MonitorFeed对象", description = "工单表")
public class MonitorFeed implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "主键ID")
    @TableId(value = "ID", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "工单单号")
    @TableField("feednumber")
    private String feedNumber;

    @ApiModelProperty(value = "工单标题")
    @TableField("feedTitle")
    private String feedTitle;

    @ApiModelProperty(value = "提交人")
    @TableField("submitUser")
    private String submitUser;

    @ApiModelProperty(value = "提交时间")
    @TableField("submitTime")
    private Date submitTime;

    @ApiModelProperty(value = "解决人")
    @TableField("solveUser")
    private String solveUser;

    @ApiModelProperty(value = "解决时间")
    @TableField("solveTime")
    private Date solveTime;

    @ApiModelProperty(value = "状态")
    @TableField("status")
    private Integer status;

    @ApiModelProperty(value = "内容")
    @TableField("content")
    private Integer content;
}
