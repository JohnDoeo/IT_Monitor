package com.gitee.pifeng.monitoring.ui.business.web.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gitee.pifeng.monitoring.ui.business.web.vo.MonitorFeedVo;
import com.gitee.pifeng.monitoring.ui.business.web.vo.MonitorHttpVo;

public interface IMonitorFeedService {
    Page<MonitorFeedVo> getMonitorFeedList(Long current, Long size, String feedNumber, String submitUser, String solveUser, Integer status);
}
