package com.gitee.pifeng.monitoring.ui.business.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gitee.pifeng.monitoring.ui.business.web.dao.IMonitorFeedDao;
import com.gitee.pifeng.monitoring.ui.business.web.entity.MonitorFeed;
import com.gitee.pifeng.monitoring.ui.business.web.service.IMonitorFeedService;
import com.gitee.pifeng.monitoring.ui.business.web.vo.MonitorFeedVo;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IMonitorFeedServiceImpl implements IMonitorFeedService {

    @Autowired
    private IMonitorFeedDao monitorFeedDao;
    @Override
    public Page<MonitorFeedVo> getMonitorFeedList(Long current, Long size,String feedNumber, String submitUser, String solveUser, Integer status) {
        IPage<MonitorFeed> ipage = new Page<>(current, size);
        LambdaQueryWrapper<MonitorFeed> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.select(MonitorFeed ::getId,MonitorFeed::getFeedNumber,MonitorFeed::getFeedTitle,
                MonitorFeed::getSubmitUser,MonitorFeed::getSubmitTime,MonitorFeed::getSolveUser,MonitorFeed::getSolveTime,
                MonitorFeed::getStatus);
        if (StringUtils.isNotBlank(feedNumber)){
            lambdaQueryWrapper.like(MonitorFeed::getFeedNumber,feedNumber);
        }
        if (StringUtils.isNotBlank(submitUser)){
            lambdaQueryWrapper.like(MonitorFeed::getSubmitUser,submitUser);
        }
        if (StringUtils.isNotBlank(solveUser)){
            lambdaQueryWrapper.like(MonitorFeed::getSolveUser,solveUser);
        }
        if (status!=null){
            lambdaQueryWrapper.like(MonitorFeed::getStatus,status);
        }
        IPage<MonitorFeed> monitorFeedPage = this.monitorFeedDao.selectPage(ipage, lambdaQueryWrapper);
        List<MonitorFeed> records = monitorFeedPage.getRecords();
        List<MonitorFeedVo> monitorFeedVos = Lists.newLinkedList();
        for (MonitorFeed record : records) {
            MonitorFeedVo monitorFeedVo = MonitorFeedVo.builder().build().convertFor(record);
            monitorFeedVos.add(monitorFeedVo);
        }
        Page<MonitorFeedVo> objectPage = new Page<>();
        objectPage.setRecords(monitorFeedVos);
        objectPage.setTotal(monitorFeedPage.getTotal());
        return objectPage;
    }
}
