package com.gitee.pifeng.monitoring.ui.business.web.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gitee.pifeng.monitoring.ui.business.web.entity.MonitorDb;
import com.gitee.pifeng.monitoring.ui.business.web.entity.MonitorFeed;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import java.util.Date;

@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@Builder
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "工单信息表现层对象")
public class MonitorFeedVo {
    @ApiModelProperty(value = "主键ID")
    private Long id;

    @ApiModelProperty(value = "工单单号")
    private String feedNumber;

    @ApiModelProperty(value = "工单标题")
    private String feedTitle;

    @ApiModelProperty(value = "提交人")
    private String submitUser;

    @ApiModelProperty(value = "提交时间")
    private Date submitTime;

    @ApiModelProperty(value = "解决人")
    private String solveUser;

    @ApiModelProperty(value = "解决时间")
    private Date solveTime;

    @ApiModelProperty(value = "状态")
    private Integer status;

    @ApiModelProperty(value = "内容")
    private String content;

    public MonitorFeedVo convertFor(MonitorFeed monitorFeed) {
        if (null != monitorFeed) {
            BeanUtils.copyProperties(monitorFeed, this);
        }
        return this;
    }
}
